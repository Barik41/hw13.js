// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.

// setTimeout це таймер який виконує якусь дію післязаданого відрізку часу,
// а setInterval це метод, що буде виконуватия якусь дію кожен заданий проміжок часу.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?

// Цей метод буде викликаний відразу після того як завершиться виконання поточного коду, тобто реальна затримка ніколи не буде рівнятися 0.
// Задана затримка нуль лише означає, що метод буде викликаний настільки швидко, настільки це можливо в календарі викликів.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?

// Бо сама функція, а особливо змінні на які вона посилаєтсья продовжуть займати пам'ять браузера.

// При запуску програми на екрані має відображатись перша картинка.
// Через 3 секунди замість неї має бути показано друга картинка.
// Ще через 3 секунди – третя.
// Ще через 3 секунди – четверта.
// Після того, як будуть показані всі картинки - цей цикл має розпочатися наново.
// Після запуску програми десь на екрані має з'явитись кнопка з написом Припинити.
// Після натискання на кнопку Припинити цикл завершується, на екрані залишається показаною та картинка, яка була там при натисканні кнопки.
// Поруч із кнопкою Припинити має бути кнопка Відновити показ, при натисканні якої цикл триває з тієї картинки, яка в даний момент показана на екрані.
// Розмітку можна змінювати, додавати потрібні класи, ID, атрибути, теги.


document.body.insertAdjacentHTML('beforeend', `<div class="timer"></div>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-stop">Припинити показ</button>`);
document.body.insertAdjacentHTML('beforeend', `<button class="btn btn-start" disabled>Відновити показ</button>`);

btnStop = document.querySelector('.btn-stop');
btnStart = document.querySelector('.btn-start');

btnStart.addEventListener('click', e => {
   if (e.target.closest('.btn-start')) {
      changeImg(count);
   }
});
btnStop.addEventListener('click', e => {
   if (e.target.closest('.btn-stop')) {
      console.log("stop");
      timer.innerHTML = 2 + ' : ' + 999;
      clearTimeout(imgTimerId);
      clearInterval(timerId);
      btnStop.setAttribute("disabled", '');
      btnStart.removeAttribute("disabled")
   }
});

const images = document.querySelectorAll('.image-to-show');
const timer = document.querySelector('.timer');
let count = 0;
let imgTimerId;
let timerId;

function changeImg() {
   if (count > images.length - 1) count = 0;
   startTime()
   images[count].classList.add('active');
   imgTimerId = setTimeout(() => {
      images[count].classList.add('fade-out');
      setTimeout(() => {
         images[count].classList.remove('active', 'fade-out');
         count++;
         clearInterval(timerId);
         changeImg()
      }, 500);
   }, 2500);
};
changeImg()


function startTime() {
   console.log("tick");
   let responseTime = new Date(Date.now() + (1000 * 3));
   let countdown = new Date();
   timerId = setInterval(function () {
      countdown.setTime(responseTime - Date.now());
      timer.innerHTML = (countdown.getUTCSeconds()) + ' : ' + countdown.getUTCMilliseconds();
   }, 10);

   btnStart.setAttribute("disabled", '');
   btnStop.removeAttribute("disabled")
};

